@extends('layouts.app')
@section('title','List company')

@section('content')

<div class="container-fluid">
        <a href="{{ route('empresas.create') }}" class="btn btn-success"> Create a new company</a>
</div>

      
             


<div class="container-fluid">
        <div class="panel panel-default">
                <div class="panel-heading">Companies list</div>
                <div class="panel-body">
                        <table class="table table-bordered">
                                        <thead>
                                                <th> Name </th>
                                                <th> Website </th>
                                                <th> Email </th>
                                                <th>  </th>
                                        </thead>
                                <tbody>
                                @foreach($empresas as $empresa)
                                <tr>
                                        <td>{{ $empresa->name}} </td>
                                        <td>{{ $empresa->site}} </td>
                                        <td>{{ $empresa->email}} </td>
                                        <td> <a href="{{ route('empresas.edit', $empresa->id) }}" class="btn btn-info"> Edit</a>
                                                {!!Form::open([
                                                        'method'=>'delete',
                                                        'route' =>['empresas.destroy',$empresa->id],
                                                        'id' => 'delete_company',
                                                    ])!!}
                                            
                                            <input type="submit" value="Delete" class="btn btn-danger" onclick="return confirm('Are you sure to delete?')"> 
                                                                                                         
                                            {!! Form::close() !!}
                                        
                                        </td>
                                </tr>
                                @endforeach
                                </tbody>
                        
                                </table>
                        {{ $empresas->links() }}
                </div>
              
        </div>
</div>
@endsection 