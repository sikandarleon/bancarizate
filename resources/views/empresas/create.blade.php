@extends('layouts.app')
@section('title','Create company')

@section('content')

<div class="container-fluid">
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
        <h1> Create company </h1>
    {!! Form::open(['route' => 'empresas.store', 'method' => 'POST','files' => true,'enctype' =>'multipart/form-data']) !!}
    <div class="row">
    
        <div class="col-xs-9 col-md-4">
            {!! Form::label('name','Name') !!}
            {!! Form::text('name',null,['class' => 'form-control' , 'requiere']) !!}
             
        </div>
         
    </div>
    <div class="row">
            
        <div class="col-xs-6 col-md-4">
            {!! Form::label('name','Email') !!}
            {!! Form::email('email',null,['class' => 'form-control' , 'requiere']) !!}
        </div>

    </div>
    <div class="row">
            
            <div class="col-xs-6 col-md-4">
                {!! Form::label('site-web','Site Web') !!}
                {!! Form::text('siteWeb',null,['class' => 'form-control' , 'requiere']) !!}
            </div>
    
        </div>
    <div class="row">
            
        <div class="col-xs-6 col-md-4">
            {!! Form::label('name','Logo') !!}
            {!! Form::file('logo', ['roles' => 'form','class' => 'filestyle', 'data-buttonBefore' =>'true', 'data-btnClass' => "btn-primary", 'data-text' => "Find logo" ]  ) !!}
        
          
         
            
        </div>
    
    </div>
        <div class="row">
            
                <div class="col-xs-6 col-md-10 btn-save" >
                        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                </div>
      
            </div>       
    
	{!! Form::close() !!}
        
    </div>
          


        
 

  

   

    


@endsection 
