@extends('layouts.app')
@section('title','Create employee')

@section('content')

<div class="container-fluid">
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
        <h1> Create employee </h1>
    {!! Form::open(['route' => 'empleados.store', 'method' => 'POST','files' => true,'enctype' =>'multipart/form-data']) !!}
    <div class="row">
    
        <div class="col-xs-9 col-md-4">
            {!! Form::label('name','Name') !!}
            {!! Form::text('name',null,['class' => 'form-control' , 'requiere']) !!}
             
        </div>
         
    </div>
    <div class="row">
    
            <div class="col-xs-9 col-md-4">
                {!! Form::label('lastname','Last Name') !!}
                {!! Form::text('last_name',null,['class' => 'form-control' , 'requiere']) !!}
                 
            </div>
             
        </div>
    <div class="row">
            
        <div class="col-xs-6 col-md-4">
            {!! Form::label('name','Email') !!}
            {!! Form::email('email',null,['class' => 'form-control' , 'requiere']) !!}
        </div>

    </div>
    <div class="row">
            
            <div class="col-xs-6 col-md-4">
                {!! Form::label('cellphone','Cellphone') !!}
                {!! Form::text('celphone',null,['class' => 'form-control' , 'requiere']) !!}
            </div>
    
    </div>
    <div class="row">
        <div class="col-xs-6 col-md-4">
            {!! Form::label('company','Company') !!}
            {!!Form::select('empresa_id', $companies, null, ['placeholder' => 'Select your company...','class' => 'form-control']) !!}
        </div>
    
    </div>
        <div class="row">
            
                <div class="col-xs-6 col-md-10 btn-save" >
                        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                </div>
      
            </div>       
    
	{!! Form::close() !!}
        
    </div>
          


        
 

  

   

    


@endsection 
