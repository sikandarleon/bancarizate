@extends('layouts.app')
@section('title','List employee')

@section('content')

<div class="container-fluid">
        <a href="{{ route('empleados.create') }}" class="btn btn-success"> Create a new employee</a>
</div>

<div class="container-fluid">
                <div class="panel panel-default">
                        <div class="panel-heading">Employees list</div>
                        <div class="panel-body">
                                <table class="table table-bordered">
                                                <thead>
                                                        <th> Name </th>
                                                        <th> Last name </th>
                                                        <th> Email </th>
                                                        <th> CellPhone </th>
                                                        <th> Company </th>
                                                </thead>
                                        <tbody>
                                        @foreach($employees as $employee)
                                        <tr>
                                                <td>{{ $employee->name}} </td>
                                                <td>{{ $employee->last_name}} </td>
                                                <td>{{ $employee->email}} </td>
                                                <td>{{ $employee->celphone}} </td>
                                                <td>{{ $employee->empresa->name}} </td>
                                                <td> <a href="{{ route('empleados.edit', $employee->id) }}" class="btn btn-info"> Edit</a>
                                                        {!!Form::open([
                                                                'method'=>'delete',
                                                                'route' =>['empleados.destroy',$employee->id],
                                                                'id' => 'delete_company',
                                                            ])!!}
                                                    
                                                    <input type="submit" value="Delete" class="btn btn-danger" onclick="return confirm('Are you sure to delete?')"> 
                                                                                                                 
                                                    {!! Form::close() !!}
                                                
                                                </td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                
                                        </table>
                                {{ $employees->links() }}
                        </div>
                      
                </div>
        </div>

      
             



@endsection 