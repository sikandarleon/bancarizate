@extends('layouts.app')
@section('title','update employee')

@section('content')

<div class="container-fluid">
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
        <h1> Update employee </h1>
    {!! Form::open(['route' =>  ['empleados.update',$employee->id], 'method' => 'PUT','files' => true,'enctype' =>'multipart/form-data']) !!}
    <div class="row">
    
        <div class="col-xs-9 col-md-4">
            {!! Form::label('name','Name') !!}
            {!! Form::text('name',$employee->name,['class' => 'form-control' , 'requiere']) !!}
             
        </div>
         
    </div>
    <div class="row">
    
            <div class="col-xs-9 col-md-4">
                {!! Form::label('lastname','Last Name') !!}
                {!! Form::text('last_name',$employee->last_name,['class' => 'form-control' , 'requiere']) !!}
                 
            </div>
             
        </div>
    <div class="row">
            
        <div class="col-xs-6 col-md-4">
            {!! Form::label('name','Email') !!}
            {!! Form::email('email',$employee->email,['class' => 'form-control' , 'requiere']) !!}
        </div>

    </div>
    <div class="row">
            
            <div class="col-xs-6 col-md-4">
                {!! Form::label('cellphone','Cellphone') !!}
                {!! Form::text('celphone',$employee->celphone,['class' => 'form-control' , 'requiere']) !!}
            </div>
    
    </div>
    <div class="row">
        <div class="col-xs-6 col-md-4">
            {!! Form::label('company','Company') !!}
            {!!Form::select('empresa_id',$companies, $employee->empresa_id, ['placeholder' => 'Select your company...','class' => 'form-control']) !!}
        </div>
    
    </div>
        <div class="row">
            
                <div class="col-xs-6 col-md-10 btn-save" >
                        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                </div>
      
            </div>       
    
	{!! Form::close() !!}
        
    </div>
          


        
 

  

   

    


@endsection 
