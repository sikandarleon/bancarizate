<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\StoreEmpresa;
use Illuminate\Support\Facades\DB;
use App\Empresa;



class EmpresaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $empresas = DB::table('empresa')->paginate(10);
        return view('empresas.index', ['empresas' => $empresas]);
        //return view('empresas.index');
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('empresas.create');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEmpresa $request)
    {   
        //dd($request);
        $company = new Empresa;
        $company->name = $request->name;
        $company->email = $request->email;
        $company->site = $request->siteWeb;

       
        $custom_file_name = time().'-'.$request->file('logo')->getClientOriginalName();
        $file = $request->logo->storeAs('company_logos', $custom_file_name);
        $company->logo = $file;
     

       
        $company->save();

        return redirect()->route('empresas.index');
        
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
     
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

       
        $empresa = Empresa::find($id);
        return view('empresas.edit', ['empresa' => $empresa]);

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $company = Empresa::find($id);
       
        if (strcmp ($company->email,$request->email) == 0){
          
            $validatedData = $request->validate([
                'name' => 'required|max:100',
                'siteWeb' => 'required|max:191',
                'logo' => 'required|image|dimensions:min_width=100,min_height=100',
              
            ]);

            $company->name = $request->name;
            $company->email = $request->email;
            $company->site = $request->siteWeb;
      
            $custom_file_name = time().'-'.$request->file('logo')->getClientOriginalName();
            $file = $request->logo->storeAs('company_logos', $custom_file_name);
            $company->logo = $file;           
            $company->save();

            return redirect()->route('empresas.index');



        }else{
            
            $validatedData = $request->validate([
                'name' => 'required|max:100',
                'email' => 'required|unique:empresa|max:191',
                'siteWeb' => 'required|max:191',
                'logo' => 'required|image|dimensions:min_width=100,min_height=100',
              
            ]);
        }
       
      

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $empresa = Empresa::find($id);
        $empresa->delete();       

        return redirect()->route('empresas.index');
        //
    }
}
