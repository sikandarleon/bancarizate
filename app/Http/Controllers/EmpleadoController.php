<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\StoreEmpleado;
use Illuminate\Support\Facades\DB;
use App\Empleado;

class EmpleadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        //$employees = DB::table('empleados')->paginate(10);
        $employees = Empleado::paginate(10);
        return view('empleados.index',['employees' => $employees]);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companies = DB::table('empresa')->pluck('name', 'id');
        return view('empleados.create', ['companies' => $companies]);
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEmpleado $request)
    {
        $employee = new Empleado($request->all());
        $employee->save();

        return redirect()->route('empleados.index');
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $employee = Empleado::find($id);
        $companies = DB::table('empresa')->pluck('name', 'id');
        return view('empleados.edit', ['employee' => $employee,'companies'=> $companies]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $employee = Empleado::find($id);
       
        if (strcmp ($employee->email,$request->email) == 0){          
       
            $employee->fill($request->all())->save();
            return redirect()->route('empleados.index');

        }else{
            
            $validatedData = $request->validate([
             
                'email' => 'required|unique:empleados|max:191',
       
              
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee = Empleado::find($id);
        $employee->delete();       

        return redirect()->route('empleados.index');
        //
    }
}
