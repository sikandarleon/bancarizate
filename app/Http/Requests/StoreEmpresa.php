<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;


class StoreEmpresa extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:100',
            'email' => 'required|unique:empresa|max:191',
            'siteWeb' => 'required|max:191',
            //'logo' => 'required',
            'logo' => 'required|image|dimensions:min_width=100,min_height=100',
            //'email' => 'required|unique:email|max:191',
            //'name' => 'required|file',
            //
        ];
    }


public function messages()
{
    return [
        'logo.dimensions' => 'The :attribute is 100x100.',
        //'price.required' => 'Añade un :attribute al producto',
        //'price.min' => 'El :attribute debe ser mínimo 0'
    ];
}

    public function attributes()    
    {
        return [
            'logo' => 'min size of the company logo',
            //'price' => 'precio de venta',
        ];
    }
}
