<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreEmpleado extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:50',
            'last_name' => 'required|max:50',
            'email' => 'required|unique:empleados|max:50',
            'celphone' => 'required|max:50',
            'empresa_id' => 'required',       
            
        ];
    }
    public function messages()
{
    return [
        
        'email.required' => 'The email must be :attribute ',
        //'price.min' => 'El :attribute debe ser mínimo 0'
    ];
}

public function attributes()    
    {
        return [
            'email' => 'unique',
            //'price' => 'precio de venta',
        ];
    }
}
