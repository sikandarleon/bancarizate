<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empleado extends Model
{
    protected $table = "empleados";
    protected $fillable = ['name','last_name','email','celphone','empresa_id'];
    protected $dates = ['created_at','updated_at'];

    public function empresa(){
    	return $this->belongsTo('App\Empresa');
    }
    //
}
