<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    protected $table = "empresa";
    protected $fillable = ['name','email','site','logo'];
    protected $dates = ['created_at','updated_at'];

    public function empleados(){
    	return $this->hasMany('App\Empleado');
    }
    //
}
